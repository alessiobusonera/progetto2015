Progetto di PR2 per l'anno accademico 2014/15.

Autori: Alessio Busonera, Guido Pili

Questo progetto comprende la realizzazione di funzione di libreria LibreOfficeCalc,
con l'ausilio di Java. Infatti le funzioni sono scritte in linguaggio java.
Una funzione Semplice, si occupa di trovare un numero random tra l'intervallo 0.0 e 1.0.
Non prende in ingresso nessun parametro.
Una funzione Complessa, invece si occupa di trovare il minimo di un campo passato come parametro,
che rispetta delle condizioni passate anche questo come parametro. L'area di ricerca inserita riguarda
un database che contiene dati di uno studente. Matricola, Cognome, Nome, Media, CFU, ed anni fuori corso.
Una funzione Custom, invece si occupa di stampare il contenuto di una file di testo esclusivamente .doc, e
lo salva in un file di testo .txt. La funzione prende in ingresso i due percorsi dei file a partire da /home.


Edit: il file presente nel repository chiamato foglioDiProva.ods non deve essere preso in considerazione.
Sono sorti dei problemi con git, e non si è cancellato. Prendere in considerazione soltato il file foglio.ods