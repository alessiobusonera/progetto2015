package it.unica.pr2.progetto2015.g48892_49002;

public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

	/**
   *@param il parametro in questione è un parametro vuoto, perché non serve per questa funzione
   *@return un numero casuale tra 0.0 e 1.0
   */
	public Object execute(Object... args) {
		
		return Math.random();
	}

	public final String getCategory() {

		return "Matematica";
	}

	public final String getHelp() {

		return "Restituisce un numero casuale tra l'intervallo 0.0 e 1.0";
	} 

	public final String getName() {
		
		return "CASUAL";
	}
}
