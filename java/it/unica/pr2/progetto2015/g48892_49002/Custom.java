package it.unica.pr2.progetto2015.g48892_49002;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Pattern;

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hwpf.extractor.WordExtractor;

public class Custom implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

  /**
   *@param args[0] contiene l'indirizzo del file di estensione doc del quale salvare il contenuto
   *@param args[1] contiene l'indirizzo del file di testo dove sarà salvato il contenuto precedentemente salvato
   *@return una stringa contentente l'intero contenuto del file .doc
   */
  public Object execute(Object... args) {

    String pathDoc = (String) args[0];    //contiene l'indirizzo del file .doc
    File fileDoc = new File(pathDoc);     
    String pathTxt = (String) args[1];    //contiene l'indirizzo del file .txt
    File fileTxt = new File(pathTxt);  
    String contenuto = "";                //contiene il risultato   
    String regex = ".+/.+\\.[doc]+";

    // controllo se il percorsi passati sono corretti
    if ( !fileDoc.isFile() ) return "Percorso file doc inesistente. Inserisci un percorso valido.";

    if ( !fileTxt.exists() ) {

      try {

        fileTxt.createNewFile();
      } catch (IOException e) {
        
        e.printStackTrace();
      }
    }
    if ( !fileTxt.isFile() ) return "Percorso file txt inesistente. Inserisci un percorso valido.";
    
    // controllo se l'espressione regolare è in un formato valido
    try {

      Pattern.compile(regex);
    } catch (PatternSyntaxException e) {

      return "Formato dell'espressione regolare non valido. Ricontrolla.";
    }

    // se sono arrivato qua, significa che l'espressione regolare ha un formato
    // corretto, e i percoso inseriti sono validi,
    // quindi posso effettuare il controllo
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(pathDoc);

    // controllo se l'matching rende falso, se così è allora l'estensione del file
    // non è corretta
    if ( !matcher.matches() ) return "Estensione del file non valida.";

    // salvo il contenuto del file in una stringa
    try {

      POIFSFileSystem mioFile = new POIFSFileSystem( new FileInputStream(pathDoc) );
      WordExtractor estrattoreWord = new WordExtractor(mioFile);
      contenuto = estrattoreWord.getText();
    } catch (IOException e) {

      e.printStackTrace();
    }

    // salvo il contenuto della stringa nel file txt passato come parametro
    try {

      PrintWriter out = new PrintWriter(fileTxt);
      out.println(contenuto);
      out.close();
    } catch (IOException e) {

      e.printStackTrace();
    }

    return contenuto;

  }

  public final String getCategory() {

    return "STAMPA&SALVA";
  }

  public final String getHelp() {

    return "Restituisce il contenuto di un file di testo in formato doc e lo salva in un file di testo formato txt";
  } 

  public final String getName() {
    
    return "DOCTOTXT";
  }
}
