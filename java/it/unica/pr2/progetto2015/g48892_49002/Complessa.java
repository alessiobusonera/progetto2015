package it.unica.pr2.progetto2015.g48892_49002;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;

public class Complessa implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

    /**
     *@param args[0] contiene una matrice contente l'area dati ( il database )
     *@param args[1] contiene il campo di ricerca del minimo
     *@param args[2] contiene una matrice che contiene i criteri di selezione
     *@return il minimo di args[1] secondo i criteri di args[2]
     */
    public Object execute (Object... args) {
        
        String[][] database = (String[][]) args[0];
        String campoDatabase = (String) args[1];
        String[][] criteri = (String[][]) args[2];

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");

        int numeroRigheDatabase = database.length;              
        int numeroColonneDatabase = database[0].length;
        int numeroCriteri = criteri[0].length;
                
        int indiceCampo = -1;       // contiene l'indice del campo di cui trovare il minimo.
        

        for ( int i = 0; i < numeroColonneDatabase; i++) {

            // utilizzo il metodo equalsIgnoreCase in modo da effettuare un confronto senza badare a differenza tra MAIUSC/minusc
            if (database[0][i].equalsIgnoreCase(campoDatabase)) {

                indiceCampo = i;
            }
        }

        if (indiceCampo == -1) return "Campo non presente. Err:504";
            
        ArrayList<Integer> indiciCriteri = new ArrayList<>(numeroCriteri); // contiene gli indici dei criteri

        for (int i = 0; i < numeroCriteri; i++) {

            indiciCriteri.add(i, -1);
        }

        int index = 0;     // usato per inserire i valore all'interno dell'ArrayList, funge da indice
        // cerco gli indici
        for ( int i = 0; i < numeroCriteri; i++) {

          for ( int j = 0; j < numeroColonneDatabase; j++) {

            if (database[0][j].equalsIgnoreCase(criteri[0][i]) ) {

              indiciCriteri.add(index, j);
              index++;
            }
          }
        }

        for (int i = 0; i < numeroCriteri; i++) {

            if ( indiciCriteri.get(i) == -1) return "Criterio non presente. Err:504";
        }
        
        // mi appoggio ad una tabella che conterrà solo 1 e 0.
        // Se contiene 1 significa che l'elemento selezionato soddisfa il criterio, 0 altrimenti.
        int[][] tabellaBooleani = new int[numeroRigheDatabase][numeroCriteri];
        
        // effettuo dei controlli per vedere come iniziano i criteri, nel caso in cui inizi diversamente da un simbolo
        for (int i = 0; i < numeroCriteri; i++) {

          if (criteri[1][i].startsWith("=") ) {

            criteri[1][i] = "=" + criteri[1][i];
          } else if ( !(criteri[1][i].startsWith("<")) && (!criteri[1][i].startsWith(">")) ) {

            criteri[1][i] = "==" + criteri[1][i];
          } else {

            criteri[1][i] = "" + criteri[1][i];
          }        
        }
        
        // controllo la condizione del criterio, nel caso venga soddisfatta, allora l'elemento di tabellaBooleani
        // prende 1.
        for ( int i = 0; i < numeroCriteri; i++) {

          for ( int j = 1; j < numeroRigheDatabase; j++) {

            String current = database[j][indiciCriteri.get(i)].concat(criteri[1][i]);
            
            try {
              if ( (Boolean)engine.eval(current) ) {

                //allora è vero
                j--;
                tabellaBooleani[j][i] = 1;
                j++;
              } else {

                //allora è falso
                j--;
                tabellaBooleani[j][i] = 0;
                j++;
              }
            } catch (ScriptException e) {}
          }
        }

        for ( int i = 0; i < numeroCriteri; i++) {
            
            if (criteri[1][i].equals("==")) {
                
                for (int j = 0; j < numeroRigheDatabase-1; j++) {
                    
                    tabellaBooleani[j][i] = 1;
                }
            }
        }
        
        int conta = 0;      // contatore per contare le occorrenze di 3 uni consecutivi
        int[] array = new int[numeroRigheDatabase-1]; // mi appoggio per fare il controllo per poi eseguire l'espressione regolare
        
        for (int i = 0; i < numeroRigheDatabase -1; i++) {
            
            for (int j = 0; j < numeroCriteri; j++) {
                
                if (tabellaBooleani[i][j] == 1) {
                    
                    conta+=1;
                }
            }
            // se il contatore vale 3 allora ho trovato 3 uno
            if ( conta == numeroCriteri) array[i] = 1;
            conta = 0;      // riazzero il contatore
        }

        String min = database[1][indiceCampo];      // contiene il primo elemento che verrà poi confrontato con gli altri
        Boolean flag = true;                        // inzialmente vero. se trovo nell'array almeno un 1, singifica che posso
                                                    // effettuare un confronto

        for ( int i = 1, j = 0;j < numeroRigheDatabase-1 && i < numeroRigheDatabase; j++, i++) {
               
            if ( array[j] == 1) {
                
                flag = false;
                try {

                    String current = database[i][indiceCampo] + " <" + min;
                    if ( (Boolean)engine.eval(current) ) {      // rende vero se l'espressione regolare è vera. Es 180 > 120 true

                      min = database[i][indiceCampo];          
                    }
                } catch (ScriptException e) {}                
            } 
        }
        
        // se flag vale ancora true, significa che non è stato trovato mai un 1, quindi il minimo deve valere 0, non il primo elemento
        if (flag) {
            
            min = "0";
        }

        Double minimo = 0.00;
        // effettuo conversione a Double per stampare il risultato senza il .0 nel caso fosse intero
        try {
            
            minimo = Double.parseDouble(min);
        } catch (NumberFormatException e) {}


        return minimo; //restuisco il risultato
    }

    public final String getCategory() {

        return "Database";
    }

    public final String getHelp() {

        return "Restituisce il valore minimo nel campo di record del database che soddisfa le condizioni specificate";
    } 

    public final String getName() {
        
        return "DB.MIN";
    }
    
}
